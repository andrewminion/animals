<?php

use App\Models\Animal;
use App\Models\Characteristics\BodyStructure;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBodyStructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_structures', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('animal_body_structure', function (Blueprint $table) {
            $table->foreignIdFor(Animal::class)->constrained();
            $table->foreignIdFor(BodyStructure::class)->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_body_structure');
        Schema::dropIfExists('body_structures');
    }
}
