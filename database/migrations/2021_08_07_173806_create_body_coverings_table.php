<?php

use App\Models\Animal;
use App\Models\Characteristics\BodyCovering;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBodyCoveringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_coverings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('animal_body_covering', function (Blueprint $table) {
            $table->foreignIdFor(Animal::class)->constrained();
            $table->foreignIdFor(BodyCovering::class)->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_body_covering');
        Schema::dropIfExists('body_coverings');
    }
}
