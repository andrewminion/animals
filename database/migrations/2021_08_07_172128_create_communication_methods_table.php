<?php

use App\Models\Animal;
use App\Models\Characteristics\CommunicationMethod;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunicationMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communication_methods', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('animal_communication_method', function (Blueprint $table) {
            $table->foreignIdFor(Animal::class)->constrained();
            $table->foreignIdFor(CommunicationMethod::class)->constrained();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_communication_method');
        Schema::dropIfExists('communication_methods');
    }
}
