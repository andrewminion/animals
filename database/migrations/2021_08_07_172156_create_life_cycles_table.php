<?php

use App\Models\Animal;
use App\Models\Characteristics\LifeCycle;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLifeCyclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('life_cycles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('animal_life_cycle', function (Blueprint $table) {
            $table->foreignIdFor(Animal::class)->constrained();
            $table->foreignIdFor(LifeCycle::class)->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_life_cycle');
        Schema::dropIfExists('life_cycles');
    }
}
