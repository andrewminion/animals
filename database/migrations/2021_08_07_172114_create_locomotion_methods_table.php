<?php

use App\Models\Animal;
use App\Models\Characteristics\LocomotionMethod;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocomotionMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locomotion_methods', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('animal_locomotion_method', function (Blueprint $table) {
            $table->foreignIdFor(Animal::class)->constrained();
            $table->foreignIdFor(LocomotionMethod::class)->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_locomotion_method');
        Schema::dropIfExists('locomotion_methods');
    }
}
