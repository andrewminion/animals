<?php

use App\Models\Classifications\Kingdom;
use App\Models\Classifications\Phylum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassificationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kingdoms', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('phyla', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Kingdom::class)->constrained();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('animal_classes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('phylum_id')->references('id')->on('phyla')->constrained();
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_classes');
        Schema::dropIfExists('phyla');
        Schema::dropIfExists('kingdoms');
    }
}
