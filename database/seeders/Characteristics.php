<?php

namespace Database\Seeders;

use App\Models\Characteristics\BodyCovering;
use App\Models\Characteristics\BodyStructure;
use App\Models\Characteristics\BreathingMethod;
use App\Models\Characteristics\CommunicationMethod;
use App\Models\Characteristics\LifeCycle;
use App\Models\Characteristics\LocomotionMethod;
use App\Models\Characteristics\Sense;
use Illuminate\Database\Seeder;

class Characteristics extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BodyCovering::factory()
            ->times(4)
            ->sequence(
                ['name' => 'Feathers'],
                ['name' => 'Fur'],
                ['name' => 'Skin'],
                ['name' => 'Scales'],
            )
            ->create();

        BodyStructure::factory()
            ->sequence(
                ['name' => 'Upright'],
            )
            ->create();

        BreathingMethod::factory()
            ->times(2)
            ->sequence(
                ['name' => 'Gills'],
                ['name' => 'Lungs'],
            )
            ->create();

        CommunicationMethod::factory()
            ->times(3)
            ->sequence(
                ['name' => 'Calling'],
                ['name' => 'Speech'],
                ['name' => 'Stamping'],
            )
            ->create();

        LifeCycle::factory()
            ->times(3)
            ->sequence(
                ['name' => 'Egg'],
                ['name' => 'Pupa'],
                ['name' => 'Larva'],
            )
            ->create();

        LocomotionMethod::factory()
            ->times(3)
            ->sequence(
                ['name' => 'Flying'],
                ['name' => 'Swimming'],
                ['name' => 'Walking'],
            )
            ->create();

        Sense::factory()
            ->times(6)
            ->sequence(
                ['name' => 'Feel'],
                ['name' => 'Sight'],
                ['name' => 'Smell'],
                ['name' => 'Taste'],
                ['name' => 'Touch'],
                ['name' => 'Other'],
            )
            ->create();
    }
}
