<?php

namespace Database\Factories\Characteristics;

use App\Models\Characteristics\LocomotionMethod;
use Illuminate\Database\Eloquent\Factories\Factory;

class LocomotionMethodFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LocomotionMethod::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
        ];
    }
}
