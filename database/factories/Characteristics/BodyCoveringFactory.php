<?php

namespace Database\Factories\Characteristics;

use App\Models\Characteristics\BodyCovering;
use Illuminate\Database\Eloquent\Factories\Factory;

class BodyCoveringFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BodyCovering::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
        ];
    }
}
