<?php

namespace Database\Factories\Characteristics;

use App\Models\Characteristics\Sense;
use Illuminate\Database\Eloquent\Factories\Factory;

class SenseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sense::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
        ];
    }
}
