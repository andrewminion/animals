<?php

namespace Database\Factories\Characteristics;

use App\Models\Characteristics\CommunicationMethod;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommunicationMethodFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CommunicationMethod::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
        ];
    }
}
