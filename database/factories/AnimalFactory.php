<?php

namespace Database\Factories;

use App\Models\Animal;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method Animal|\Illuminate\Support\Collection<Animal> create(attributes = [], ?Model parent = null)
 * @method \Illuminate\Support\Collection<Animal> createMany(iterable records)
 * @method Animal createOne(attributes = [])
 * @method Animal|\Illuminate\Support\Collection<Animal> make(attributes = [], ?Model parent = null)
 * @method Animal makeOne(attributes = [])
 */class AnimalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Animal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'common_name' => $this->faker->words(2, true),
            'scientific_name' => $this->faker->words(2, true),
            'length' => $this->faker->randomNumber(3).' inches',
            'width' => $this->faker->randomNumber(3).' inches',
            'height' => $this->faker->randomNumber(3).' inches',
            'weight' => $this->faker->randomNumber(3).' pounds',
            'notes' => $this->faker->sentences(3, true),
            'diet' => $this->faker->sentences(3, true),
            'color' => $this->faker->hexColor(),
        ];
    }
}
