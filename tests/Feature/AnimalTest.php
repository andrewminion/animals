<?php

namespace Tests\Feature;

use App\Http\Livewire\Animal as AnimalComponent;
use App\Models\Animal;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Livewire\Livewire;
use Tests\TestCase;

class AnimalTest extends TestCase
{
    use RefreshDatabase;

    public function test_data_submission()
    {
        /** @var Animal $animal */
        $animal = Animal::factory()->make();

        $this->assertDatabaseCount(Animal::class, 0);

        Livewire::test(AnimalComponent::class)
            ->fill([
                'animal.common_name' => $animal->common_name,
                'animal.scientific_name' => $animal->scientific_name,
                'animal.length' => $animal->length,
                'animal.width' => $animal->width,
                'animal.height' => $animal->height,
                'animal.weight' => $animal->weight,
                'animal.notes' => $animal->notes,
                'animal.diet' => $animal->diet,
                'animal.color' => $animal->color,
                'photo' => UploadedFile::fake()->image('test.jpg'),
            ])
            ->call('submit')
            ->assertHasNoErrors()
            ->assertRedirect(route('animals.list'));

        $this->assertDatabaseHas(Animal::class, [
            'common_name' => $animal->common_name,
            'scientific_name' => $animal->scientific_name,
            'length' => $animal->length,
            'width' => $animal->width,
            'height' => $animal->height,
            'weight' => $animal->weight,
            'notes' => $animal->notes,
            'diet' => $animal->diet,
            'photo' => 'creature-power-disks/test.jpg',
            'color' => $animal->color,
        ]);
    }
}
