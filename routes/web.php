<?php

use App\Http\Livewire;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])
    ->group(function () {

    Route::get('dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::name('animals.')
        ->prefix('animals/')
        ->group(function () {
            Route::get('/', Livewire\AnimalList::class)->name('list');
            Route::get('/new', Livewire\Animal::class)->name('new');
            Route::get('/{animal}', Livewire\Animal::class)->name('show');
        });

    Route::name('characteristics.')
        ->prefix('characteristics/')
        ->group(function () {
            Route::get('body-coverings/', Livewire\Characteristics\BodyCoveringList::class)->name('body-coverings.list');
            Route::get('body-coverings/new', Livewire\Characteristics\BodyCovering::class)->name('body-coverings.new');
            Route::get('body-coverings/{model}', Livewire\Characteristics\BodyCovering::class)->name('body-coverings.show');

            Route::get('body-structures/', Livewire\Characteristics\BodyStructureList::class)->name('body-structures.list');
            Route::get('body-structures/new', Livewire\Characteristics\BodyStructure::class)->name('body-structures.new');
            Route::get('body-structures/{model}', Livewire\Characteristics\BodyStructure::class)->name('body-structures.show');

            Route::get('breathing-methods/', Livewire\Characteristics\BreathingMethodList::class)->name('breathing-methods.list');
            Route::get('breathing-methods/new', Livewire\Characteristics\BreathingMethod::class)->name('breathing-methods.new');
            Route::get('breathing-methods/{model}', Livewire\Characteristics\BreathingMethod::class)->name('breathing-methods.show');

            Route::get('communication-methods/', Livewire\Characteristics\CommunicationMethodList::class)->name('communication-methods.list');
            Route::get('communication-methods/new', Livewire\Characteristics\CommunicationMethod::class)->name('communication-methods.new');
            Route::get('communication-methods/{model}', Livewire\Characteristics\CommunicationMethod::class)->name('communication-methods.show');

            Route::get('life-cycles/', Livewire\Characteristics\LifeCycleList::class)->name('life-cycles.list');
            Route::get('life-cycles/new', Livewire\Characteristics\LifeCycle::class)->name('life-cycles.new');
            Route::get('life-cycles/{model}', Livewire\Characteristics\LifeCycle::class)->name('life-cycles.show');

            Route::get('locomotion-methods/', Livewire\Characteristics\LocomotionMethodList::class)->name('locomotion-methods.list');
            Route::get('locomotion-methods/new', Livewire\Characteristics\LocomotionMethod::class)->name('locomotion-methods.new');
            Route::get('locomotion-methods/{model}', Livewire\Characteristics\LocomotionMethod::class)->name('locomotion-methods.show');

            Route::get('senses/', Livewire\Characteristics\SenseList::class)->name('senses.list');
            Route::get('senses/new', Livewire\Characteristics\Sense::class)->name('senses.new');
            Route::get('senses/{model}', Livewire\Characteristics\Sense::class)->name('senses.show');
        });
    });
