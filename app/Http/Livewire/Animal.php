<?php

namespace App\Http\Livewire;

use App\Models;
use Livewire\Component;
use Livewire\WithFileUploads;

class Animal extends Component
{
    use WithFileUploads;

    public Models\Animal $animal;
    public array $bodyCoverings = [];
    public array $bodyStructures = [];
    public array $breathingMethods = [];
    public array $communicationMethods = [];
    public array $lifeCycles = [];
    public array $locomotionMethods = [];
    public array $senses = [];
    /** @var \Livewire\TemporaryUploadedFile $photo */
    public $photo;

    protected $rules = [
        'animal.common_name' => 'string|nullable',
        'animal.scientific_name' => 'string|nullable',
        'animal.length' => 'string|nullable',
        'animal.width' => 'string|nullable',
        'animal.height' => 'string|nullable',
        'animal.weight' => 'string|nullable',
        'animal.notes' => 'string|nullable',
        'animal.diet' => 'string|nullable',
        'animal.color' => 'nullable|string',

        'bodyCoverings' => 'nullable|array',
        'bodyCoverings.*' => 'boolean',
        'bodyStructures' => 'nullable|array',
        'bodyStructures.*' => 'boolean',
        'breathingMethods' => 'nullable|array',
        'breathingMethods.*' => 'boolean',
        'communicationMethods' => 'nullable|array',
        'communicationMethods.*' => 'boolean',
        'lifeCycles' => 'nullable|array',
        'lifeCycles.*' => 'boolean',
        'locomotionMethods' => 'nullable|array',
        'locomotionMethods.*' => 'boolean',
        'senses' => 'nullable|array',
        'senses.*' => 'boolean',

        'photo' => 'nullable|image|max:8192',
    ];

    public function mount($animal = null)
    {
        if ($animal) {
            $this->animal = $animal;

            $this->bodyCoverings = array_fill_keys($this->animal->body_coverings->pluck('id')->toArray(), 1);
            $this->bodyStructures = array_fill_keys($this->animal->body_structures->pluck('id')->toArray(), 1);
            $this->breathingMethods = array_fill_keys($this->animal->breathing_methods->pluck('id')->toArray(), 1);
            $this->communicationMethods = array_fill_keys($this->animal->communication_methods->pluck('id')->toArray(), 1);
            $this->lifeCycles = array_fill_keys($this->animal->lifecycles->pluck('id')->toArray(), 1);
            $this->locomotionMethods = array_fill_keys($this->animal->locomotion_methods->pluck('id')->toArray(), 1);
            $this->senses = array_fill_keys($this->animal->senses->pluck('id')->toArray(), 1);
        } else {
            $this->animal = new Models\Animal();
        }
    }

    public function render()
    {
        return view('livewire.animal');
    }

    public function submit()
    {
        $validated = $this->validate();

        if (! data_get($this->animal, 'id')) {
            $this->animal = new Models\Animal();
        }

        if (isset($this->photo)) {
            $photo = $this->photo->storePubliclyAs('creature-power-disks', $this->photo->getClientOriginalName(), 's3');
            $validated['animal']['photo'] = $photo;
        }

        $this->animal->fill($validated['animal']);
        $this->animal->save();

        $this->animal->body_coverings()->sync(array_keys(array_filter($validated['bodyCoverings'])));
        $this->animal->body_structures()->sync(array_keys(array_filter($validated['bodyStructures'])));
        $this->animal->breathing_methods()->sync(array_keys(array_filter($validated['breathingMethods'])));
        $this->animal->communication_methods()->sync(array_keys(array_filter($validated['communicationMethods'])));
        $this->animal->lifecycles()->sync(array_keys(array_filter($validated['lifeCycles'])));
        $this->animal->locomotion_methods()->sync(array_keys(array_filter($validated['locomotionMethods'])));
        $this->animal->senses()->sync(array_keys(array_filter($validated['senses'])));

        $this->redirectRoute('animals.list');
    }
}
