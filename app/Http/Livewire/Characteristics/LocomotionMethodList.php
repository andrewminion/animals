<?php

namespace App\Http\Livewire\Characteristics;

use App\Models\Characteristics\LocomotionMethod;

class LocomotionMethodList extends BaseList
{
    public string $model = LocomotionMethod::class;
}
