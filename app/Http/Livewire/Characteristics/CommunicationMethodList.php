<?php

namespace App\Http\Livewire\Characteristics;

use App\Models\Characteristics\CommunicationMethod;

class CommunicationMethodList extends BaseList
{
    public string $model = CommunicationMethod::class;
}
