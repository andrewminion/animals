<?php

namespace App\Http\Livewire\Characteristics;

use App\Models\Characteristics\BodyStructure;

class BodyStructureList extends BaseList
{
    public string $model = BodyStructure::class;
}
