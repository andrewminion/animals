<?php

namespace App\Http\Livewire\Characteristics;

use App\Models;
use Livewire\Component;

class CommunicationMethod extends Component
{
    public Models\Characteristics\CommunicationMethod $model;

    protected $rules = [
        'model.name' => 'required|string',
    ];

    public function mount($model = null)
    {
        if ($model) {
            $this->model = $model;
        } else {
            $this->model = new Models\Characteristics\CommunicationMethod();
        }
    }

    public function render()
    {
        return view('livewire.characteristics.base');
    }

    public function submit()
    {
        $validated = $this->validate();

        if (! data_get($this->model, 'id')) {
            $this->model = new Models\Characteristics\CommunicationMethod();
        }

        $this->model->fill($validated['model']);
        $this->model->save();

        $this->redirectRoute('characteristics.communication-methods.list');
    }
}
