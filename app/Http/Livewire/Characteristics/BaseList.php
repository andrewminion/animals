<?php

namespace App\Http\Livewire\Characteristics;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class BaseList extends DataTableComponent
{
    public string $model;

    public function columns(): array
    {
        return [
            Column::make('Name')
                ->searchable()
                ->sortable(),
            Column::make('Actions')
                ->format(function ($value, $column, $row) {
                    return view('components.models.edit', ['route' => route(Str::of(request()->route()->getName())->replace('list', 'show')->__toString(), $row)]);
                }),
        ];
    }

    public function query(): Builder
    {
        return (new $this->model)::query();
    }
}
