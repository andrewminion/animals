<?php

namespace App\Http\Livewire\Characteristics;

use App\Models\Characteristics\BodyCovering;

class BodyCoveringList extends BaseList
{
    public string $model = BodyCovering::class;
}
