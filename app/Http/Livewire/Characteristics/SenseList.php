<?php

namespace App\Http\Livewire\Characteristics;

use App\Models\Characteristics\Sense;

class SenseList extends BaseList
{
    public string $model = Sense::class;
}
