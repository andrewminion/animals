<?php

namespace App\Http\Livewire\Characteristics;

use App\Models\Characteristics\BreathingMethod;

class BreathingMethodList extends BaseList
{
    public string $model = BreathingMethod::class;
}
