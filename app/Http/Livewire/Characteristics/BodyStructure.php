<?php

namespace App\Http\Livewire\Characteristics;

use App\Models;
use Livewire\Component;

class BodyStructure extends Component
{
    public Models\Characteristics\BodyStructure $model;

    protected $rules = [
        'model.name' => 'required|string',
    ];

    public function mount($model = null)
    {
        if ($model) {
            $this->model = $model;
        } else {
            $this->model = new Models\Characteristics\BodyStructure();
        }
    }

    public function render()
    {
        return view('livewire.characteristics.base');
    }

    public function submit()
    {
        $validated = $this->validate();

        if (! data_get($this->model, 'id')) {
            $this->model = new Models\Characteristics\BodyStructure();
        }

        $this->model->fill($validated['model']);
        $this->model->save();

        $this->redirectRoute('characteristics.body-structures.list');
    }
}
