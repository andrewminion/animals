<?php

namespace App\Http\Livewire\Characteristics;

use App\Models;
use Livewire\Component;

class LocomotionMethod extends Component
{
    public Models\Characteristics\LocomotionMethod $model;

    protected $rules = [
        'model.name' => 'required|string',
    ];

    public function mount($model = null)
    {
        if ($model) {
            $this->model = $model;
        } else {
            $this->model = new Models\Characteristics\LocomotionMethod();
        }
    }

    public function render()
    {
        return view('livewire.characteristics.base');
    }

    public function submit()
    {
        $validated = $this->validate();

        if (! data_get($this->model, 'id')) {
            $this->model = new Models\Characteristics\LocomotionMethod();
        }

        $this->model->fill($validated['model']);
        $this->model->save();

        $this->redirectRoute('characteristics.locomotion-methods.list');
    }
}
