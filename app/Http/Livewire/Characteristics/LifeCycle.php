<?php

namespace App\Http\Livewire\Characteristics;

use App\Models;
use Livewire\Component;

class LifeCycle extends Component
{
    public Models\Characteristics\LifeCycle $model;

    protected $rules = [
        'model.name' => 'required|string',
    ];

    public function mount($model = null)
    {
        if ($model) {
            $this->model = $model;
        } else {
            $this->model = new Models\Characteristics\LifeCycle();
        }
    }

    public function render()
    {
        return view('livewire.characteristics.base');
    }

    public function submit()
    {
        $validated = $this->validate();

        if (! data_get($this->model, 'id')) {
            $this->model = new Models\Characteristics\LifeCycle();
        }

        $this->model->fill($validated['model']);
        $this->model->save();

        $this->redirectRoute('characteristics.life-cycles.list');
    }
}
