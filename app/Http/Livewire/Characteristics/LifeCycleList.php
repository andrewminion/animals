<?php

namespace App\Http\Livewire\Characteristics;

use App\Models\Characteristics\LifeCycle;

class LifeCycleList extends BaseList
{
    public string $model = LifeCycle::class;
}
