<?php

namespace App\Http\Livewire;

use App\Models;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class AnimalList extends DataTableComponent
{
    public function columns(): array
    {
        return [
            Column::make('Common Name')
                ->searchable()
                ->sortable(),
            Column::make('Scientific Name')
                ->searchable()
                ->sortable()
                ->format(function ($value, $column, $row) {
                    return '<em>'.$value.'</em>';
                })
                ->asHtml(),
            Column::make('Actions')
                ->format(function ($value, $column, $row) {
                    return view('components.models.edit', ['route' => route('animals.show', $row)]);
                }),
        ];
    }

    public function query(): Builder
    {
        return Models\Animal::query();
    }
}
