<?php

namespace App\Models\Characteristics;

use App\Models\Animal;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Characteristics\BreathingMethod
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\Characteristics\BreathingMethodFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|BreathingMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BreathingMethod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BreathingMethod query()
 * @method static \Illuminate\Database\Eloquent\Builder|BreathingMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BreathingMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BreathingMethod whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BreathingMethod whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BreathingMethod extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Animal[] */
    public function anmials()
    {
        $this->belongsToMany(Animal::class);
    }
}
