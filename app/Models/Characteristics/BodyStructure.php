<?php

namespace App\Models\Characteristics;

use App\Models\Animal;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Characteristics\BodyStructure
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\Characteristics\BodyStructureFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyStructure newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BodyStructure newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BodyStructure query()
 * @method static \Illuminate\Database\Eloquent\Builder|BodyStructure whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyStructure whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyStructure whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyStructure whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BodyStructure extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Animal[] */
    public function anmials()
    {
        $this->belongsToMany(Animal::class);
    }
}
