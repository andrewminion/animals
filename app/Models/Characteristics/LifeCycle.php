<?php

namespace App\Models\Characteristics;

use App\Models\Animal;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Characteristics\LifeCycle
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\Characteristics\LifeCycleFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|LifeCycle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LifeCycle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LifeCycle query()
 * @method static \Illuminate\Database\Eloquent\Builder|LifeCycle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LifeCycle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LifeCycle whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LifeCycle whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LifeCycle extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Animal[] */
    public function anmials()
    {
        $this->belongsToMany(Animal::class);
    }
}
