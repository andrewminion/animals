<?php

namespace App\Models\Characteristics;

use App\Models\Animal;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Characteristics\LocomotionMethod
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\Characteristics\LocomotionMethodFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|LocomotionMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocomotionMethod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocomotionMethod query()
 * @method static \Illuminate\Database\Eloquent\Builder|LocomotionMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocomotionMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocomotionMethod whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocomotionMethod whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LocomotionMethod extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Animal[] */
    public function anmials()
    {
        $this->belongsToMany(Animal::class);
    }
}
