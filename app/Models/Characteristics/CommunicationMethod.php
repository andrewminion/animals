<?php

namespace App\Models\Characteristics;

use App\Models\Animal;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Characteristics\CommunicationMethod
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\Characteristics\CommunicationMethodFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|CommunicationMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CommunicationMethod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CommunicationMethod query()
 * @method static \Illuminate\Database\Eloquent\Builder|CommunicationMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CommunicationMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CommunicationMethod whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CommunicationMethod whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CommunicationMethod extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Animal[] */
    public function anmials()
    {
        $this->belongsToMany(Animal::class);
    }
}
