<?php

namespace App\Models\Characteristics;

use App\Models\Animal;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Characteristics\BodyCovering
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Animal[] $anmials
 * @property-read int|null $anmials_count
 * @method static \Database\Factories\Characteristics\BodyCoveringFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyCovering newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BodyCovering newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BodyCovering query()
 * @method static \Illuminate\Database\Eloquent\Builder|BodyCovering whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyCovering whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyCovering whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyCovering whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BodyCovering extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Animal[] */
    public function anmials()
    {
        return $this->belongsToMany(Animal::class);
    }
}
