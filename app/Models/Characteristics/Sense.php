<?php

namespace App\Models\Characteristics;

use App\Models\Animal;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Characteristics\Sense
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\Characteristics\SenseFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Sense newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sense newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sense query()
 * @method static \Illuminate\Database\Eloquent\Builder|Sense whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sense whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sense whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sense whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Sense extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Animal[] */
    public function anmials()
    {
        $this->belongsToMany(Animal::class)->withPivot('strength');
    }
}
