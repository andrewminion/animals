<?php

namespace App\Models\Classifications;

use App\Models\Animal;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AnimalClass
 *
 * @property int $id
 * @property int $phylum_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Classifications\Phylum $phylum
 * @method static \Illuminate\Database\Eloquent\Builder|AnimalClass newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnimalClass newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnimalClass query()
 * @method static \Illuminate\Database\Eloquent\Builder|AnimalClass whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnimalClass whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnimalClass whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnimalClass wherePhylumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnimalClass whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AnimalClass extends Model
{
    use HasFactory;

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\App\Models\Phylum */
    public function phylum()
    {
        return $this->belongsTo(Phylum::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\Animal[] */
    public function animals()
    {
        $this->hasMany(Animal::class);
    }
}
