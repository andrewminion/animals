<?php

namespace App\Models\Classifications;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Kingdom
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Kingdom newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kingdom newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kingdom query()
 * @method static \Illuminate\Database\Eloquent\Builder|Kingdom whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kingdom whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kingdom whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kingdom whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Kingdom extends Model
{
    use HasFactory;

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\Phylum[] */
    public function phyla()
    {
        $this->hasMany(Phylum::class);
    }
}
