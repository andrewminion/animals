<?php

namespace App\Models\Classifications;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Phylum
 *
 * @property int $id
 * @property int $kingdom_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Classifications\Kingdom $kingdom
 * @method static \Illuminate\Database\Eloquent\Builder|Phylum newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Phylum newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Phylum query()
 * @method static \Illuminate\Database\Eloquent\Builder|Phylum whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phylum whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phylum whereKingdomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phylum whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phylum whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Phylum extends Model
{
    use HasFactory;

    protected $table = 'phyla';

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\App\Models\Kingdom */
    public function kingdom()
    {
        return $this->belongsTo(Kingdom::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\AnimalClass[] */
    public function classes()
    {
        $this->hasMany(AnimalClass::class);
    }
}
