<?php

namespace App\Models;

use App\Models\Characteristics\BodyCovering;
use App\Models\Characteristics\BodyStructure;
use App\Models\Characteristics\BreathingMethod;
use App\Models\Characteristics\CommunicationMethod;
use App\Models\Characteristics\LifeCycle;
use App\Models\Characteristics\LocomotionMethod;
use App\Models\Characteristics\Sense;
use App\Models\Classifications\AnimalClass;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Animal
 *
 * @property int $id
 * @property int|null $animal_class_id
 * @property string|null $common_name
 * @property string|null $scientific_name
 * @property string|null $length
 * @property string|null $width
 * @property string|null $height
 * @property string|null $weight
 * @property string|null $notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $diet
 * @property string|null $photo
 * @property string|null $color
 * @property-read \Illuminate\Database\Eloquent\Collection|BodyCovering[] $body_coverings
 * @property-read int|null $body_coverings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|BodyStructure[] $body_structures
 * @property-read int|null $body_structures_count
 * @property-read \Illuminate\Database\Eloquent\Collection|BreathingMethod[] $breathing_methods
 * @property-read int|null $breathing_methods_count
 * @property-read AnimalClass $class
 * @property-read \Illuminate\Database\Eloquent\Collection|CommunicationMethod[] $communication_methods
 * @property-read int|null $communication_methods_count
 * @property-read \Illuminate\Database\Eloquent\Collection|LifeCycle[] $lifecycles
 * @property-read int|null $lifecycles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|LocomotionMethod[] $locomotion_methods
 * @property-read int|null $locomotion_methods_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Sense[] $senses
 * @property-read int|null $senses_count
 * @method static \Database\Factories\AnimalFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Animal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Animal query()
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereAnimalClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereCommonName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereDiet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereScientificName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Animal whereWidth($value)
 * @mixin \Eloquent
 */
class Animal extends Model
{
    use HasFactory;

    protected $fillable = [
        'common_name',
        'scientific_name',
        'length',
        'width',
        'height',
        'weight',
        'notes',
        'diet',
        'photo',
        'color',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\App\Models\Classifications\AnimalClass */
    public function class()
    {
        return $this->belongsTo(AnimalClass::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Characteristics\BodyCovering[] */
    public function body_coverings()
    {
        return $this->belongsToMany(BodyCovering::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Characteristics\BodyStructure[] */
    public function body_structures()
    {
        return $this->belongsToMany(BodyStructure::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Characteristics\BreathingMethod[] */
    public function breathing_methods()
    {
        return $this->belongsToMany(BreathingMethod::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Characteristics\CommunicationMethod[] */
    public function communication_methods()
    {
        return $this->belongsToMany(CommunicationMethod::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Characteristics\LifeCycle[] */
    public function lifecycles()
    {
        return $this->belongsToMany(LifeCycle::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Characteristics\LocomotionMethod[] */
    public function locomotion_methods()
    {
        return $this->belongsToMany(LocomotionMethod::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Characteristics\Sense[] */
    public function senses()
    {
        return $this
            ->belongsToMany(Sense::class)
            ->withPivot('strength');
    }
}
