<div>
    <x-slot name="header">
        <x-h2>{{ $model->name ?? 'New' }}</x-h2>
    </x-slot>

    <x-form wire:submit.prevent="submit" class="p-6 bg-white border-b border-gray-200 drop-shadow-sm rounded-md">

        <x-form-input
            wire:model.defer="model.name"
            name="naame"
            autofocus
            label="Name"
        />

        <x-form-submit />

    </x-form>
</div>
