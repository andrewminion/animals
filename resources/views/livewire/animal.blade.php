<div>
    <x-slot name="header">
        <x-h2>{{ $animal->common_name ?? 'New Animal' }}</x-h2>
    </x-slot>

    <x-form wire:submit.prevent="submit" class="p-6 bg-white border-b border-gray-200 drop-shadow-sm rounded-md md:grid gap-4 grid-cols-2">

        <x-form-input
            wire:model.defer="animal.common_name"
            autofocus
            name="common_name"
            label="Common Name"
        />

        <x-form-input
            class="italic"
            wire:model.defer="animal.scientific_name"
            name="scientific_name"
            label="Scientific Name"
        />

        <x-form-input
            wire:model.defer="animal.length"
            name="length"
            label="Length"
        />

        <x-form-input
            wire:model.defer="animal.width"
            name="width"
            label="Width"
        />

        <x-form-input
            wire:model.defer="animal.height"
            name="height"
            label="Height"
        />

        <x-form-input
            wire:model.defer="animal.weight"
            name="weight"
            label="Weight"
        />

        <x-form-textarea
            wire:model.defer="animal.notes"
            name="notes"
            label="Notes"
            rows="5"
        />

        <x-form-textarea
            wire:model.defer="animal.diet"
            name="diet"
            label="Diet"
            rows="5"
        />

        <x-form-group name="bodyCoverings[]" label="{{ __('Body Coverings') }}" inline>
            @foreach (\App\Models\Characteristics\BodyCovering::all()->pluck('name', 'id')->toArray() as $value => $label)
                <x-form-checkbox
                    wire:model.defer="bodyCoverings.{{ $value }}"
                    name="bodyCoverings[]"
                    value="1"
                    label="{{ $label }}"
                    />
            @endforeach
        </x-form-group>

        <x-form-group name="bodyStructures[]" label="{{ __('Body Structures') }}" inline>
            @foreach (\App\Models\Characteristics\BodyStructure::all()->pluck('name', 'id')->toArray() as $value => $label)
                <x-form-checkbox
                    wire:model.defer="bodyStructures.{{ $value }}"
                    name="bodyStructures[]"
                    value="1"
                    label="{{ $label }}"
                    />
            @endforeach
        </x-form-group>

        <x-form-group name="breathingMethods[]" label="{{ __('Breathing Methods') }}" inline>
            @foreach (\App\Models\Characteristics\BreathingMethod::all()->pluck('name', 'id')->toArray() as $value => $label)
                <x-form-checkbox
                    wire:model.defer="breathingMethods.{{ $value }}"
                    name="breathingMethods[]"
                    value="1"
                    label="{{ $label }}"
                    />
            @endforeach
        </x-form-group>

        <x-form-group name="communicationMethods[]" label="{{ __('Communication Methods') }}" inline>
            @foreach (\App\Models\Characteristics\CommunicationMethod::all()->pluck('name', 'id')->toArray() as $value => $label)
                <x-form-checkbox
                    wire:model.defer="communicationMethods.{{ $value }}"
                    name="communicationMethods[]"
                    value="1"
                    label="{{ $label }}"
                    />
            @endforeach
        </x-form-group>

        <x-form-group name="lifeCycles[]" label="{{ __('Lifecycles') }}" inline>
            @foreach (\App\Models\Characteristics\LifeCycle::all()->pluck('name', 'id')->toArray() as $value => $label)
                <x-form-checkbox
                    wire:model.defer="lifeCycles.{{ $value }}"
                    name="lifeCycles[]"
                    value="1"
                    label="{{ $label }}"
                    />
            @endforeach
        </x-form-group>

        <x-form-group name="locomotionMethods[]" label="{{ __('Locomotion Methods') }}" inline>
            @foreach (\App\Models\Characteristics\LocomotionMethod::all()->pluck('name', 'id')->toArray() as $value => $label)
                <x-form-checkbox
                    wire:model.defer="locomotionMethods.{{ $value }}"
                    name="locomotionMethods[]"
                    value="1"
                    label="{{ $label }}"
                    />
            @endforeach
        </x-form-group>

        <x-form-group name="senses[]" label="{{ __('Senses') }}" inline>
            @foreach (\App\Models\Characteristics\Sense::all()->pluck('name', 'id')->toArray() as $value => $label)
                <x-form-checkbox
                    wire:model.defer="senses.{{ $value }}"
                    name="senses[]"
                    value="1"
                    label="{{ $label }}"
                    />
            @endforeach
        </x-form-group>

        <div>
            <p class="mt-4 text-gray-700">Creature Power Disk Color</p>
            <x-color-picker wire:model="animal.color" :color="$animal->color" />
        </div>

        @if ($animal->photo)
            <div>
                <p class="mt-4 text-gray-700">Current Photo</p>
                <img src="{{ asset_cdn($animal->photo) }}" />
            </div>
        @endif

        <x-form-input
            type="file"
            wire:model="photo"
            name="photo"
            label="{{ isset($animal->photo) ? 'Replace Photo' : 'Add Photo' }}"
            />

        <x-form-submit />

    </x-form>
</div>
