@props(['color' => '#ffffff'])

<div class="mt-4">
    <script src="https://unpkg.com/vanilla-picker@2"></script>

    <div
        x-data="{ color: '{{ $color }}' }"
        x-init="
            picker = new Picker({
                parent: $refs.button,
                popup: 'bottom',
                onDone: rawColor => {
                    color = rawColor.hex;
                    $dispatch('input', color)
                }
            });
        "
        wire:ignore
        {{ $attributes }}
    >
        <span class="p-2 cursor-pointer" x-text="color"  x-ref="button" :style="`background: ${color}`"></span>
    </div>
</div>
