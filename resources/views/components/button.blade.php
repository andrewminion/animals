<a class="px-4 py-2 bg-green-500 text-white rounded-md hover:bg-green-700 focus:border-green-300 active:bg-green-900 transition-all" href="{{ $href }}">
    {{ $slot }}
</a>
