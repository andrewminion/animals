<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="p-8 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <h2 class="text-2xl">Animals</h2>

                @php
                    $count = \App\Models\Animal::count();
                @endphp
                <p class="mt-4">Currently you have {{ __(':count :animal', ['count' => $count, 'animal' => Str::plural('animal', $count)]) }} in the database.</p>

                <p class="mt-4">
                    <x-button href="{{ route('animals.list') }}">
                        See Animals
                    </x-button>
                </p>
            </div>
        </div>
    </div>
</x-app-layout>
